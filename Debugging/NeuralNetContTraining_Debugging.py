# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 13:36:59 2020

@author: mgley

Debugged version of NeuralNetContTraining_Debugging, includes some stnadardization/normalization
of the columns code. 
"""

from NeuralNetClass_Debugging import Neural_Net                                      
import pandas as pd  
import numpy as np  
from sklearn.preprocessing import StandardScaler
#import argparse        

#Input paramters into the model 


# =============================================================================
# parser = argparse.ArgumentParser(description='Neural Network Training')
# parser.add_argument('--filename',type = str, help='filename')
# parser.add_argument('--D_in',type = int, help='Input Dimension')
# parser.add_argument('--H',  type = int, help='Hidden Layer')
# parser.add_argument('--D_out', type = int, help='Ouput Dimension')
# parser.add_argument('--lr' , type = float, help='learning rate') 
# parser.add_argument('--beta1' , type = float, help='learning rate')  
# parser.add_argument('--beta2' , type = float, help='learning rate') 
# parser.add_argument('--M' , type = float , help = 'momentum') 
# parser.add_argument('--slurmid' , type = float , help='job id')
# 
# args = parser.parse_args()      
# 
# NN_object = Neural_Net(None, args.D_in , args.H , args.D_out , args.lr ,args.beta1, args.beta2 , args.M, args.slurmid)
# NN_object.df = pd.read_csv(args.filename)
# =============================================================================

#Intialize the neural network object's fields

NN_object = Neural_Net(None, 195, 53, 1, 0.00007, 0.92 , 0.92 , 0.96, 90001) 
NN_object.df = pd.read_csv('snap3_training_wrong_predictions_dataset.csv')         
sample_ids = pd.Series(range(0,len(NN_object.df)))
sample_ids = sample_ids.to_frame() 
sample_ids.columns = ["sample_id"] 


NN_object.df = NN_object.df.to_numpy()
#columns=[i for i in range(1, NN_object.df.shape[1] - 1)]
scaler = StandardScaler()
scaler.fit(NN_object.df[:,1:196])
NN_object.df[:,1:196] = scaler.transform(NN_object.df[:,1:196])


NN_object.df = pd.DataFrame(NN_object.df)
print(NN_object.df)
NN_object.df = NN_object.df.merge(sample_ids, left_index = True , right_index = True)  
print(NN_object.df)
NN_object.df.rename(columns={ NN_object.df.columns[0]: "split" }, inplace = True)
print(NN_object.df)
NN_object.df.rename(columns={NN_object.df.columns[196]: "PMD" }, inplace = True)
print(NN_object.df)
dataframe = NN_object.df



#removed_data = NN_object.df[(NN_object.df.snap_predictions == "Non-neutral") & (NN_object.df.PMD == 0)]
#NN_object.df = NN_object.df.drop(NN_object.df[(NN_object.df.snap_predictions == "Non-neutral") & (NN_object.df.PMD == 0)].index)



""" Method which trains on the data for 200 rounds. Finds the best accuracy epoch and trains
for that much more divided by 2 epochs. Keeps training until reaches maximal accuracy. """
def continuous_training():
    testing_accuracy_array = []  
    pos_recall_array = []
    neg_recall_array = []
    pos_pres_array = []
    neg_pres_array = []
    balanc_acc_array = []
    MCC_array = [] 
    f1_array = [] 
    ROC_AUC_array = []
    p_r_AUC_array = []
    true_labels = []
    prob_scores = []
    fp = 0
    tp= 0 
    tn = 0
    fn = 0
#    NN_object.df = NN_object.df.sample(frac = 1) 
 #   kfold = KFold(10, False)     
  #  for train, test in kfold.split(NN_object.df):
    for split_id in range(1, 11):
        NN_object.df = NN_object.df.sample(frac = 1) 
        #Dataframe = NN_object.df
        split = NN_object.df[NN_object.df.split == split_id]
        train = NN_object.df[NN_object.df.split != split_id]
        # Split the dataset into training and testing
        x_test_tensor , y_test_tensor, training_set , validation_set = NN_object.split(split, train)
    
        # Balance the validation and training datasets
        training_rand, validation_rand = NN_object.balance(training_set , validation_set)  
    
        # Create the model using the appropriate data dimensions
        model = NN_object.model(NN_object.D_in, NN_object.H, NN_object.D_out)
    
        round_ = 0  
        new_max_index = 0    
        max_index = 0 
        max_acc = 0
        epochs = 0
        new_max_acc = 1  
        continueTraining = True 
        validation_accuracy = []
        new_validation_accuracy = []  
        while continueTraining: 
            round_ += 1
            training_rand = training_rand.sample(frac = 1) 
            validation_rand = validation_rand.sample(frac = 1) 
            
            # when training epoch is less than 200. Train and find accuracy
            if round_<= 200: 
                acr = NN_object.training(round_ , model, training_rand, validation_rand)  
                validation_accuracy.append(acr)
                
            # At the 200th epoch determine check if split index is 0. If yes
            # stop, if no detemrine extra epoch number
            if round_ == 200 :
                max_acc = max(validation_accuracy)
                max_index = np.argmax(validation_accuracy) 
                print("The current round is " + str(round_))
                print("The max_Index is " + str(max_index))
                print("The max_acc is " + str(max_acc))
                split_max_index = int(max_index / 2)
                epochs = 200 + split_max_index
                if (split_max_index == 0): 
                    epochs = 300
                
            # if round > 200 continue training by the amount determined
            # in the previous conditional statement
            if round_ > 200 and round_ <= epochs: 
                acr = NN_object.training(round_ , model, training_rand , validation_rand)  
                new_validation_accuracy.append(acr)
            # When rounds equals epochs, determine if more training is necessary and how much
            if round_ == epochs : 
                new_max_acc = max(new_validation_accuracy)
                new_max_index = np.argmax(new_validation_accuracy)
                split_max_index = int(new_max_index / 2)
                new_validation_accuracy = []
                print("The current round is " + str(round_))
                print("new_max_acc is " + str(new_max_acc))
                print("max accuracy is " + str(max_acc))
                # If max accuracy is at the first or 2nd epoch
                # No more training is necessary
                if (split_max_index == 0) : 
                    break
                # If the new max accuracy <= the older max accuracy
                # exit the loop
                if new_max_acc <= max_acc:
                    continueTraining = False
                    print(epochs)
                    print(continueTraining)
                # Else make the variable max_acc assigned new_max_acc
                else:
                    print(continueTraining)
                    max_acc = new_max_acc
                    epochs += split_max_index
        x_test = x_test_tensor    
        y_test = y_test_tensor
        
        # Find accuracy on test set for each fold of CV
        NN_object.testing(split_id, model, x_test, y_test)
        test , prob, true_p, false_p ,true_n , false_n, pos_recall , neg_recall , pos_pres , neg_pres , balan_acc , MCC , f1 ,ROC_AUC, pres_recall_AUC, accuracy = NN_object.testing(split_id, model, x_test, y_test)
        # Calculate overall true positives,false positives, true negatives and false negatives
        # Add each performance metric from a fold to an array of performance metrics.
        tp += true_p
        fp += false_p
        tn += true_n
        fn += false_n
        true_labels.extend(test)
        prob_scores.extend(prob)
        testing_accuracy_array.append(accuracy)
        pos_recall_array.append(pos_recall)
        neg_recall_array.append(neg_recall)
        pos_pres_array.append(pos_pres)
        neg_pres_array.append(neg_pres)
        balanc_acc_array.append(balan_acc)
        MCC_array.append(MCC)
        f1_array.append(f1)
        ROC_AUC_array.append(ROC_AUC)
        p_r_AUC_array.append(pres_recall_AUC)
        
    # Print out all the final performance metrics
    NN_object.Results(true_labels , prob_scores, tp , fp , tn , fn, pos_recall_array,neg_recall_array,pos_pres_array,neg_pres_array,balanc_acc_array,MCC_array,f1_array,ROC_AUC_array,p_r_AUC_array,testing_accuracy_array)
    print(testing_accuracy_array)

    
if __name__ == "__main__":
    continuous_training()                 