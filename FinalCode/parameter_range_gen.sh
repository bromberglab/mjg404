#!/bin/bash

#SBATCH --partition=main
#SBATCH --job-name=arrayrun
#SBATCH --time=02:00:00
#SBATCH --error=FinalMerged.err
for i in $(seq 0.9 0.01 0.95)
do
  for j in $(seq 0.9 0.01 0.99)
  do
     echo "--filename snap3_training_dataset_michael.csv --D_in 195 --H 50 --D_out 1 --lr 0.0001 --beta1 $i --beta2 $j --M 0.9" >> parameters.txt
  done 
done
