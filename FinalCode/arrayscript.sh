#!/bin/bash

#SBATCH --partition=main
#SBATCH --job-name=arrayrun
#SBATCH --time=02:00:00
#SBATCH --output=arrayrun_%A_%a.out
#SBATCH --error=arrayrun_%A_%a.err
#SBATCH --array=1-10
#SBATCH --ntasks=1
#SBATCH --mail-user=mjg404@rutgers.edu


export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/home/$USER/libffi/lib/pkgconfig
export LDFLAGS=-L/home/$USER/libffi/lib64/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/$USER/libffi/lib64/

parameters=$(sed -n ${SLURM_ARRAY_TASK_ID}p parameter_file.txt)
python3.8 NeuralNetContTraining.py $parameters --slurmid ${SLURM_JOB_ID}



