#!/bin/bash

#SBATCH --partition=main
#SBATCH --job-name=arrayrun
#SBATCH --time=02:00:00
#SBATCH --error=FinalMerged.err
touch Acc_Results.txt
for i in {1..10}
do 
     Merged=$(cat Acc_Results.txt arrayrun_49789_$i.out >> Acc_Results.txt)
done
echo $Merged

    
