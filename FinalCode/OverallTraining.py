# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 01:08:54 2020

@author: Michael Gleyzer
"""

from OverallTrainingClass import Neural_Net                                    
import pandas as pd  
from joblib import dump

NN_object = Neural_Net(None, 195, 53, 1, 0.00007, 0.92 , 0.92 , 0.96, 90001) 
NN_object.df = pd.read_csv('snap3_training_dataset_splits.csv')

def continuous_training():
    NN_object.df = NN_object.df.sample(frac = 1)
    data_training =  NN_object.balance_PreProc(NN_object.df)
    epoch = 0
    model = NN_object.model(NN_object.D_in,NN_object.H,NN_object.D_out)
    for epoch in range(400) : 
        final_trained_model = NN_object.training(epoch , model, data_training)
    filename = open("model_original.model", "wb")
    dump(final_trained_model, filename)
    
if __name__ == "__main__":
    continuous_training()
    
    