# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 14:32:51 2020

@author: Michael Gleyzer
"""

from sklearn.utils import resample                                             
from sklearn.model_selection import train_test_split                
import torch                                      
import pandas as pd                 
import numpy as np               
import statistics    
import sklearn.metrics as metrics
import math

class Neural_Net: 
    device = torch.device("cpu")    
    
    """ Intialize Neural Net object based on Sample Size , Number of input layers, output layers, hidden layers
        and learning rate """   
           
    def __init__(self,df,D_in,H, D_out, learning_rate, beta1 , beta2, momentum, slurmid):
        self.df = None
        self.D_in = D_in
        self.H = H
        self.D_out = D_out
        self.learning_rate = learning_rate
        self.beta1 = beta1
        self.beta2 = beta2
        self.momentum = momentum
        self.slurmid = slurmid
        
    def balance_PreProc(self, data): 
            data = data.drop(data.columns[[0, 1 , 2 , 3]], axis=1)
            print(data.shape)
            print(data)
            
            Class_Amounts = {'Neutral':0 , 'Mild':0 , 'Moderate':0 , 'Severe':0}
            Class_Amounts['Neutral'] = len(data[data.PMD==0])
            Class_Amounts['Mild'] = len(data[data.PMD==0.3])   
            Class_Amounts['Moderate'] = len(data[data.PMD==0.6])
            Class_Amounts['Severe'] = len(data[data.PMD==1])
            
            data_set_neutral = data[data.PMD==0]
            data_set_mild = data[data.PMD==0.3]
            data_set_moderate = data[data.PMD==0.6]
            data_set_severe = data[data.PMD==1]
            
            largest_index = np.argmax(Class_Amounts.values())
            
            if (largest_index == 0) : 
                data_majority_class = data_set_neutral
                data_majority_upsampled = resample(data_majority_class, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_mild_upsampled = resample(data_set_mild, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_moderate_upsampled = resample(data_set_moderate, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_severe_upsampled = resample(data_set_severe, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                balanced_training = pd.concat([data_majority_upsampled ,data_set_mild_upsampled, 
                                               data_set_moderate_upsampled , data_set_severe_upsampled ])
            elif(largest_index == 1):
                data_majority_class = data_set_mild
                data_majority_upsampled = resample(data_majority_class, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_neutral_upsampled = resample(data_set_neutral, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_moderate_upsampled = resample(data_set_moderate, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_severe_upsampled = resample(data_set_severe, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                balanced_training = pd.concat([data_majority_upsampled ,data_set_neutral_upsampled, 
                                               data_set_moderate_upsampled , data_set_severe_upsampled ])
            elif(largest_index == 2):
                data_majority_class = data_set_moderate
                data_majority_upsampled = resample(data_majority_class, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_neutral_upsampled = resample(data_set_neutral, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_mild_upsampled = resample(data_set_mild, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_severe_upsampled = resample(data_set_severe, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                balanced_training = pd.concat([data_majority_upsampled ,data_set_mild_upsampled, 
                                               data_set_neutral_upsampled , data_set_severe_upsampled ])
            elif(largest_index == 3): 
                data_majority_class = data_set_severe
                data_majority_upsampled = resample(data_majority_class, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_neutral_upsampled = resample(data_set_neutral, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_mild_upsampled = resample(data_set_mild, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                data_set_moderate_upsampled = resample(data_set_moderate, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority_class))
                balanced_training = pd.concat([data_majority_upsampled ,data_set_mild_upsampled, 
                                               data_set_moderate_upsampled , data_set_neutral_upsampled ])
                
            data_shuffle = balanced_training.sample(frac=1) 
            print(len(data_shuffle[data_shuffle.PMD==0]))
            print(len(data_shuffle[data_shuffle.PMD==0.3]))
            print(len(data_shuffle[data_shuffle.PMD==0.6]))
            print(len(data_shuffle[data_shuffle.PMD==1])) 
            
            
            return data_shuffle
        
              
    def model(self,D_in,H,D_out):
        model = torch.nn.Sequential(
                         torch.nn.Linear(self.D_in, self.H),
                         torch.nn.ReLU(),
                         torch.nn.Linear(self.H, self.D_out),
                         torch.nn.Sigmoid()
                        )  
        return model
    
    def training(self,epoch, model, data_shuffled):  
                    device = torch.device("cpu")
                    
                    # Mean squared error loss
                    loss_fn = torch.nn.MSELoss(reduction='sum')
                 
                    
                    # Take training dataset and convert both the features and reponse values to tensors
                    x_training = data_shuffled.iloc[ :, 0: 195] 
                    y_training = data_shuffled.PMD
            
                    X_train_tensor = torch.tensor(x_training.values ,dtype = torch.float, device = device)
                    y_train_tensor = torch.tensor(y_training.values ,dtype = torch.float, device = device)
                    y_train_tensor = torch.unsqueeze(y_train_tensor, 1)
                    
                    # Forward pass: compute predicted y by passing x to the model, with momentum included. Module objects
                    # override the __call__ operator so you can call them like functions. When
                    # doing so you pass a Tensor of input data to the Module and it produces
                    # a Tensor of output data  
                    optimizer = torch.optim.Adam(model.parameters(), lr= self.learning_rate, betas=(self.beta1, self.beta2), eps = 1.0e-08 , weight_decay  = 0 ,amsgrad = False)
                    #optimizer = torch.optim.SGD(model.parameters(), lr=self.learning_rate , momentum = self.momentum)
                    y_pred = model(X_train_tensor)
                    # Compute and print loss. We pass Tensors containing the predicted and true
                    # values of y, and the loss function returns a Tensor containing the
                    # loss.         
                    

                    loss = loss_fn(y_pred, y_train_tensor)
                    rmse_loss = math.sqrt(loss) 
                    
                    if epoch % 100 == 10 or epoch % 100 == 99:
                        print(epoch, rmse_loss)

                    # Zero the gradients before running the backward pass.
                    optimizer.zero_grad()

                    # Backward pass: compute gradient of the loss with respect to all the learnable
                    # parameters of the model. Internally, the parameters of each Module are stored
                    # in Tensors with requires_grad=True, so this call will compute gradients for
                    # all learnable parameters in the model.
                    loss.backward()
                    
                    #Performs a step of gradient descent on the relevant parameters       
                    optimizer.step()
                    
                    # Convert all values in y_pred to 0's or 1's by using 0.5 as a threshold value
                    

                    return model 
    
    