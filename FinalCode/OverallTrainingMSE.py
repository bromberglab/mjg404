# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 14:29:35 2020

@author: Michael Gleyzer
"""

from OverallTrainingMSE_Class import Neural_Net                                    
import pandas as pd  
from joblib import dump

NN_object = Neural_Net(None, 195, 55, 1, 0.00007, 0.93 , 0.97 , 0.96, 90001) 
NN_object.df = pd.read_csv('snap3_training_dataset_splits_15_additional_features_plus_snap_and_pmd.csv.gz')
NN_object.df.PMD = NN_object.df.PMD_detailed.map({'neutral': 0, 'mild': 0.3, 'moderate': 0.6, 'severe': 1})


def continuous_training():
    NN_object.df = NN_object.df.sample(frac = 1)
    data_training =  NN_object.balance_PreProc(NN_object.df)
    epoch = 0
    model = NN_object.model(NN_object.D_in,NN_object.H,NN_object.D_out)
    for epoch in range(400) : 
        final_trained_model = NN_object.training(epoch , model, data_training)
    filename = open("model_new_fixed_195_MSE.model", "wb")
    dump(final_trained_model, filename)
    
if __name__ == "__main__":
    continuous_training()
    