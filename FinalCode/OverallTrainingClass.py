# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 01:19:23 2020

@author: Michael Gleyzer
"""
from sklearn.utils import resample                                             
from sklearn.model_selection import train_test_split                
import torch                                      
import pandas as pd                 
import numpy as np               
import statistics    
import sklearn.metrics as metrics
import math

class Neural_Net: 
    device = torch.device("cpu")    
    
    """ Intialize Neural Net object based on Sample Size , Number of input layers, output layers, hidden layers
        and learning rate """   
           
    def __init__(self,df,D_in,H, D_out, learning_rate, beta1 , beta2, momentum, slurmid):
        self.df = None
        self.D_in = D_in
        self.H = H
        self.D_out = D_out
        self.learning_rate = learning_rate
        self.beta1 = beta1
        self.beta2 = beta2
        self.momentum = momentum
        self.slurmid = slurmid
        
    def balance_PreProc(self, data): 
            data = data.drop(data.columns[[0]], axis=1)
        
            data_majority = data[data.PMD==1]
            data_minority = data[data.PMD==0]

            if(len(data_majority) < len(data_minority)):
                minority = data_minority
                data_minority = data_majority
                data_majority = minority

            data_majority_upsampled = resample(data_majority, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(data_majority))

            data_minority_upsampled = resample(data_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples= 2 * len(data_majority))  # reproducible results
            # reproducible results
            # Balance Training and Validation     

            balanced_training = pd.concat([data_minority_upsampled, data_majority_upsampled])
                
            # Shuffle Training and validation data
            data_shuffle = balanced_training.sample(frac=1) 
            return data_shuffle
        
              
    def model(self,D_in,H,D_out):
        model = torch.nn.Sequential(
                         torch.nn.Linear(self.D_in, self.H),
                         torch.nn.ReLU(),
                         torch.nn.Linear(self.H, self.D_out),
                         torch.nn.Sigmoid()
                        )  
        return model
    
    def training(self,epoch, model, data_shuffled):  
                    device = torch.device("cpu")
                    
                    # Mean squared error loss
                    loss_fn = torch.nn.MSELoss(reduction='sum')
                 
                    
                    # Take training dataset and convert both the features and reponse values to tensors
                    x_training = data_shuffled.iloc[ :, 0: (data_shuffled.shape[1] - 1)] 
                    y_training = data_shuffled.PMD
            
                    X_train_tensor = torch.tensor(x_training.values ,dtype = torch.float, device = device)
                    y_train_tensor = torch.tensor(y_training.values ,dtype = torch.float, device = device)
                    y_train_tensor = torch.unsqueeze(y_train_tensor, 1)
                    
                    # Forward pass: compute predicted y by passing x to the model, with momentum included. Module objects
                    # override the __call__ operator so you can call them like functions. When
                    # doing so you pass a Tensor of input data to the Module and it produces
                    # a Tensor of output data  
                    optimizer = torch.optim.Adam(model.parameters(), lr= self.learning_rate, betas=(self.beta1, self.beta2), eps = 1.0e-08 , weight_decay  = 0 ,amsgrad = False)
                    #optimizer = torch.optim.SGD(model.parameters(), lr=self.learning_rate , momentum = self.momentum)
                    y_pred = model(X_train_tensor)
                    # Compute and print loss. We pass Tensors containing the predicted and true
                    # values of y, and the loss function returns a Tensor containing the
                    # loss.         
                    

                    loss = loss_fn(y_pred, y_train_tensor)
                    
                    if epoch % 100 == 10 or epoch % 100 == 99:
                        print(epoch, loss.item())

                    # Zero the gradients before running the backward pass.
                    optimizer.zero_grad()

                    # Backward pass: compute gradient of the loss with respect to all the learnable
                    # parameters of the model. Internally, the parameters of each Module are stored
                    # in Tensors with requires_grad=True, so this call will compute gradients for
                    # all learnable parameters in the model.
                    loss.backward()
                    
                    #Performs a step of gradient descent on the relevant parameters       
                    optimizer.step()
                    
                    # Convert all values in y_pred to 0's or 1's by using 0.5 as a threshold value
                    

                    return model 
    
    
        