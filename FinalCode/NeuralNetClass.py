# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 00:30:47 2020

@author: mgley
"""
from sklearn.utils import resample                                             
from sklearn.model_selection import train_test_split                
import torch                                      
import pandas as pd                 
import numpy as np               
import statistics    
import sklearn.metrics as metrics
import math
#import matplotlib.pyplot as plt

""" Given two vectors: predicted labels and true labels. 
    Calculates accuracy """
    
def binary_acc(y_prediction, y_testing):

    correct_results_sum = (y_prediction == y_testing).sum().float() 
    acc = correct_results_sum/y_testing.shape[0]
    acc = torch.round(acc * 100) 
    
    return acc

"""Calculates all the relevant performance metrics from each training iteration"""
def ConfusionMatrix(y_prediction , y_test_pred , y_testing): 
    
    pred = y_prediction.numpy()
    pred = pred.transpose()
    pred = pred.astype('int') 
    pred = pred.squeeze(axis = 0)
    pred = pred.tolist()
    
    prob = y_test_pred.detach().numpy()
    prob = prob.transpose() 
    prob = prob.squeeze(axis = 0)
    prob = prob.tolist()
 
    
    test = y_testing.numpy()
    test = test.transpose()
    test = test.astype('int')
    test = test.squeeze(axis = 0)
    test = test.tolist()
    
    Results_matrix = metrics.confusion_matrix(test , pred)
    tp = Results_matrix[1][1]
    fp = Results_matrix[0][1]
    tn = Results_matrix[0][0]
    fn = Results_matrix[1][0]
    print(Results_matrix) 
        

    ROC_AUC = metrics.roc_auc_score(test, prob) 
    pos_recall = (Results_matrix[1][1] / (Results_matrix[1][1] + Results_matrix[1][0])) * 100
    neg_recall = (Results_matrix[0][0] / (Results_matrix[0][0] + Results_matrix[0][1])) * 100
    pos_pres = (Results_matrix[1][1] / (Results_matrix[1][1] + Results_matrix[0][1])) * 100
    neg_pres = (Results_matrix[0][0] / (Results_matrix[0][0] + Results_matrix[1][0])) * 100
    balan_acc = (pos_recall + neg_recall) / 2          
    MCC = metrics.matthews_corrcoef(test , pred)       
    f1 = metrics.f1_score(test , pred)  
    precision, recall, thresholds = metrics.precision_recall_curve(test, prob)
    pres_recall_AUC = metrics.auc(recall, precision)
    accuracy = metrics.accuracy_score(test, pred)
        
    if ((Results_matrix[1][1] == 0 and Results_matrix[1][0] == 0)):
        ROC_AUC = "No value due to all negatives"
    if((Results_matrix[1][1] ==0 and Results_matrix[0][1] == 0 )): 
        pos_pres = 0
        MCC = 0
    if((Results_matrix[0][0] ==0 and Results_matrix[1][0] == 0 )):
        neg_pres = 0 
        MCC = 0 
        
    if ((Results_matrix[0][0] == 0 and Results_matrix[0][1] == 0)):
        ROC_AUC = "no value due to all positives"
    
    return test , prob , tp , fp , tn , fn, pos_recall , neg_recall , pos_pres , neg_pres , balan_acc , MCC , f1,ROC_AUC, pres_recall_AUC, accuracy
    

""" Class that creates a feedforward neural net, does all the necessary data
processing, trains the neural net and tests the neural net."""

class Neural_Net: 
    device = torch.device("cpu")    
    
    """ Intialize Neural Net object based on Sample Size , Number of input layers, output layers, hidden layers
        and learning rate """   
           
    def __init__(self,df,D_in,H, D_out, learning_rate, beta1 , beta2, momentum, slurmid):
        self.df = None
        self.D_in = D_in
        self.H = H
        self.D_out = D_out
        self.learning_rate = learning_rate
        self.beta1 = beta1
        self.beta2 = beta2
        self.momentum = momentum
        self.slurmid = slurmid
        
    """ Split data into training and testing dataset,  
     then split training dataset into validation and training """
     
    def split(self,split , train): 
            device = torch.device("cpu")
            total_training = train
            total_training = train.drop(split.columns[[0]], axis=1)
            total_training = total_training.sample(frac = 1)
            y_total_training = total_training.PMD
            X_total_training = total_training.iloc[: , 0:195]
            
            
            # Take testing dataset and convert both the features and reponse values to tensors      
            testing_set = split
            testing_set = split.drop(split.columns[[0]], axis=1)
            y_test = testing_set.PMD  
            x_test = testing_set.iloc[: , 0:195]
            
            x_test_tensor = torch.tensor(x_test.values ,dtype = torch.float, device = device)
            y_test_tensor = torch.tensor(y_test.values ,dtype = torch.float, device = device)
            y_test_tensor = torch.unsqueeze(y_test_tensor, 1)  
            
            # Split total training dataset into validation and testing dataset
            # Allocate 10% of the total data to validation and 80% to training
            split_test_size = 0.10
            X_training , X_validation , y_training , y_validation = train_test_split(X_total_training.values, y_total_training.values, test_size = split_test_size)
            
            # Convert all the relevant datasets to dataframes         
            X_training = pd.DataFrame(data=X_training,
            index=[i for i in range(X_training.shape[0])],
            columns=['f'+str(i) for i in range(X_training.shape[1])])
            
            y_training = pd.DataFrame(data=y_training,
            index=[i for i in range(y_training.shape[0])],
            columns=['PMD'])
            
            X_validation = pd.DataFrame(data=X_validation,
            index=[i for i in range(X_validation.shape[0])],
            columns=['f'+str(i) for i in range(X_validation.shape[1])])
            
            y_validation = pd.DataFrame(data=y_validation,
            index=[i for i in range(y_validation.shape[0])],
            columns=['PMD'])
            

            
            # Merge the feature matrix and response vectors together to get the validation and training datasets   
            training_set = X_training.merge(y_training, left_index = True , right_index = True)
            validation_set = X_validation.merge(y_validation , left_index = True , right_index = True) 
            return x_test_tensor , y_test_tensor, training_set , validation_set
        
    """ Balance both validation and training datasets """  
     
    def balance(self, training , validation): 
        
            training_set_majority = training[training.PMD==1]
            training_set_minority = training[training.PMD==0]

            if(len(training_set_majority) < len(training_set_minority)):
                minority = training_set_minority
                training_set_minority = training_set_majority
                training_set_majority = minority
            
            validation_set_majority = validation[validation.PMD==1]
            validation_set_minority = validation[validation.PMD==0]
         
            if(len(validation_set_majority) < len(validation_set_minority)):
                minority = validation_set_minority
                validation_set_minority = validation_set_majority
                validation_set_majority = minority
            

            training_majority_upsampled = resample(training_set_majority, 
                                  replace=True,     # sample with replacement
                                  n_samples= 2 * len(training_set_majority))
            
            validation_majority_upsampled =  resample(validation_set_majority, 
                                  replace=True,     # sample with replacement
                                  n_samples=2 * len(validation_set_majority)) # reproducible results

            training_minority_upsampled = resample(training_set_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples= 2 * len(training_set_majority))  # reproducible results
            
            validation_minority_upsampled =  resample(validation_set_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples= 2 * len(validation_set_majority)) 
            # reproducible results
            # Balance Training and Validation     

            balanced_training = pd.concat([training_minority_upsampled, training_majority_upsampled])
            balanced_validation = pd.concat([validation_minority_upsampled, validation_majority_upsampled])   
                
            # Shuffle Training and validation data
            training_shuffle = balanced_training.sample(frac=1) 
            validation_shuffle = balanced_validation.sample(frac=1) 
            return training_shuffle , validation_shuffle
        
    """ Method to create the initial model Sequential Feed-Forward Neural Net which
     1) Multiplies by matrix of weights
     2) Applies ReLU() activation function
     3) Multiplies by second matrix of weights
     4) Apply Sigmoid function """
            
    def model(self,D_in,H,D_out):
        model = torch.nn.Sequential(
                         torch.nn.Linear(self.D_in, self.H),
                         torch.nn.ReLU(),
                         torch.nn.Linear(self.H, self.D_out),
                         torch.nn.Sigmoid()
                        )  
        return model
    """ Method to train the Neural Net using the data """
    
    def training(self,epoch, model, training_shuffled , validation_shuffled):  
                    device = torch.device("cpu")
                    
                    # Mean squared error loss
                    loss_fn = torch.nn.MSELoss(reduction='sum')
                 
                    
                    # Take training dataset and convert both the features and reponse values to tensors
                    x_training = training_shuffled.iloc[ :, 0:195]
                    y_training = training_shuffled.PMD
            
                    X_train_tensor = torch.tensor(x_training.values ,dtype = torch.float, device = device)
                    y_train_tensor = torch.tensor(y_training.values ,dtype = torch.float, device = device)
                    y_train_tensor = torch.unsqueeze(y_train_tensor, 1)
                    
                    # Forward pass: compute predicted y by passing x to the model, with momentum included. Module objects
                    # override the __call__ operator so you can call them like functions. When
                    # doing so you pass a Tensor of input data to the Module and it produces
                    # a Tensor of output data  
                    optimizer = torch.optim.Adam(model.parameters(), lr= self.learning_rate, betas=(self.beta1, self.beta2), eps = 1.0e-08 , weight_decay  = 0 ,amsgrad = False)
                    #optimizer = torch.optim.SGD(model.parameters(), lr=self.learning_rate , momentum = self.momentum)
                    y_pred = model(X_train_tensor)

                    # Compute and print loss. We pass Tensors containing the predicted and true
                    # values of y, and the loss function returns a Tensor containing the
                    # loss.         
                    

                    loss = loss_fn(y_pred, y_train_tensor)
                    
                    if epoch % 100 == 10 or epoch % 100 == 99:
                        print(epoch, loss.item())

                    # Zero the gradients before running the backward pass.
                    optimizer.zero_grad()

                    # Backward pass: compute gradient of the loss with respect to all the learnable
                    # parameters of the model. Internally, the parameters of each Module are stored
                    # in Tensors with requires_grad=True, so this call will compute gradients for
                    # all learnable parameters in the model.
                    loss.backward()
                    
                    #Performs a step of gradient descent on the relevant parameters       
                    optimizer.step()
                    zeros = torch.zeros([len(training_shuffled), 1], dtype=torch.int32)
                    ones = torch.ones([len(training_shuffled), 1], dtype=torch.int32)
                    
                    # Convert all values in y_pred to 0's or 1's by using 0.5 as a threshold value
                    y_pred = torch.where(y_pred >= 0.5 , ones, zeros)
                    
                    x_validation = validation_shuffled.iloc[ :, 0:195] 
                    y_validation = validation_shuffled.PMD
                    X_validate_tensor = torch.tensor(x_validation.values ,dtype = torch.float, device = device)
                    y_validate_tensor = torch.tensor(y_validation.values ,dtype = torch.float, device = device)
                    y_validate_tensor = torch.unsqueeze(y_validate_tensor, 1)    
                     
                    y_pred_validation = model(X_validate_tensor)    
                
                    zero = torch.zeros([len(validation_shuffled), 1], dtype=torch.int32)
                    one = torch.ones([len(validation_shuffled), 1], dtype=torch.int32)
                    y_pred_validation = torch.where(y_pred_validation > 0.5 , one, zero)  
                    acr = np.asscalar(binary_acc(y_pred_validation, y_validate_tensor).numpy())   

                    return acr
                
    """ Method which applies the optimized model to the testing set and measures the accuracy """ 
              
    def testing(self, model, x_test_tensor, y_test_tensor): 
        
            # Apply best model to the testing data
            # Predict y_test values             
            y_test_pred = model(x_test_tensor)      
            zero = torch.zeros([len(x_test_tensor), 1], dtype=torch.int32)       
            one = torch.ones([len(x_test_tensor), 1], dtype=torch.int32) 
            y_pred_testing = torch.where(y_test_pred > 0.5 , one, zero)
            true , prob , tp , fp , tn , fn, pos_recall , neg_recall , pos_pres , neg_pres , balan_acc , MCC , f1, ROC_AUC, pres_recall_AUC, accuracy = ConfusionMatrix(y_pred_testing ,y_test_pred, y_test_tensor)
            return true, prob , tp , fp , tn , fn, pos_recall , neg_recall , pos_pres , neg_pres , balan_acc , MCC , f1, ROC_AUC,pres_recall_AUC, accuracy
            
     
    """ Method to print out the results """ 
    
    def Results(self,true_labels , prob_scores, tp , fp , tn , fn, pos_recall,neg_recall,pos_prec,neg_prec,b_acc,MCC_array,F1,ROC_AUC,p_r_AUC , testing_accuracy):
        #Calculate all the relevant performance metrics
        total_pos_recall = tp / (tp + fn)
        total_neg_recall = tn / (fp + tn)
        total_pos_prec = tp /(tp + fp)
        total_neg_prec = tn / (tn + fn) 
        total_balan_acc = (total_pos_recall + total_neg_recall) / 2
        total_MCC = ((tp * tn) - (fp * fn)) / math.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fp))
        total_F1 = 2 / ((1 / total_pos_recall) + (1 / total_pos_prec))
        total_accuracy = (tp + tn) / (tp + tn + fp + fn)
        
        #Calculate area under roc cruve and plot roc curve
# =============================================================================
        ROC_AUC = metrics.roc_auc_score(true_labels, prob_scores)
#         fpr, tpr, thresholds = metrics.roc_curve(true_labels, prob_scores)
#         plt.plot(fpr, tpr)
#         plt.title("ROC Curve")
#         plt.xlabel("False Positive Rate")
#         plt.ylabel("True Positive Rate")
#         plt.show()
# =============================================================================
        
        #Calculate area udner precision-recall curve and plot precision-recall curve
# =============================================================================
        precision, recall, thresholds = metrics.precision_recall_curve(true_labels, prob_scores)
        pres_recall_AUC = metrics.auc(recall, precision)
#         plt.plot(recall , precision)
#         plt.xlim(0, 1)
#         plt.ylim(0, 1)
#         plt.gca().set_aspect('equal', adjustable='box')
#         plt.draw()
#         plt.title("Precision-Recall Curve")
#         plt.xlabel("Precision")
#         plt.ylabel("Recall")
#         plt.show()
# =============================================================================
        
        #Print out all the performance metrics and initial input parameters
        print("$Parameters and ConfusionMatrix: " + str(tp) + ", " + str(fp) + ", " + str(tn) + ", " + str(fn) + 
              ", " + str(self.D_in) + ", " + str(self.H) + ", " + str(self.D_out) + ", " + str(self.learning_rate) + ", " +
                     str(self.beta1) + ", " + str(self.beta2) + ", " + str(self.momentum) + ", " + str(self.slurmid) + ", " + str(statistics.mean(testing_accuracy)) + 
                    ", " + str(total_accuracy) + ", " + str(total_pos_recall) + ", " + str(total_neg_recall) +
                     ", " + str(total_pos_prec) + ", " + str(total_neg_prec) + ", " + str(total_balan_acc) + ", "
                     + str(total_MCC) + ", " + str(total_F1)  + ", " + str(ROC_AUC) + ", " + str(pres_recall_AUC))
        print("Overall positive recall is " + str(total_pos_recall))
        print("Overall negative recall is " + str(total_neg_recall))
        print("Overall positive precision is " + str(total_pos_prec))
        print("Overall negative precision is " + str(total_neg_prec))
        print("Overall balanced accuracy is " + str(total_balan_acc))
        print("Overall MCC is " + str(total_MCC))
        print("Overall F1 is " + str(total_F1))
        print("Overall ROC AUC is " + str(ROC_AUC))
        print("Overall Pres_recall_AUC is " + str(pres_recall_AUC))
        print("Overall accuracy is " + str(total_accuracy))
        print("Accuracy: sd " + str(((statistics.stdev(testing_accuracy)) / math.sqrt(10))))
        print("Positive recall: sd " + str(((statistics.stdev(pos_recall)) / math.sqrt(10))))
        print("Negative recall: sd " + str(((statistics.stdev(neg_recall)) / math.sqrt(10))))
        print("Positive Precision: sd " + str(((statistics.stdev(pos_prec)) / math.sqrt(10))))
        print("Negative Precision: sd " + str(((statistics.stdev(neg_prec)) / math.sqrt(10))))
        print("Balanced Accuracy: sd " + str(((statistics.stdev(b_acc)) / math.sqrt(10))))
        print("MCC: sd " + str(((statistics.stdev(MCC_array)) / math.sqrt(10))))
        print("F1 score: sd " + str(((statistics.stdev(F1)) / math.sqrt(10))))