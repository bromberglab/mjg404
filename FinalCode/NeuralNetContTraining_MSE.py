# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:01:22 2020

@author: Michael Gleyzer
"""

from NeuralNetClass_MSE import Neural_Net                                     
import pandas as pd  
import numpy as np  
#import argparse        

#Input paramters into the model 


# =============================================================================
# parser = argparse.ArgumentParser(description='Neural Network Training')
# parser.add_argument('--filename',type = str, help='filename')
# parser.add_argument('--D_in',type = int, help='Input Dimension')
# parser.add_argument('--H',  type = int, help='Hidden Layer')
# parser.add_argument('--D_out', type = int, help='Ouput Dimension')
# parser.add_argument('--lr' , type = float, help='learning rate') 
# parser.add_argument('--beta1' , type = float, help='learning rate')  
# parser.add_argument('--beta2' , type = float, help='learning rate') 
# parser.add_argument('--M' , type = float , help = 'momentum') 
# parser.add_argument('--slurmid' , type = float , help='job id')
# 
# args = parser.parse_args()      
# 
# NN_object = Neural_Net(None, args.D_in , args.H , args.D_out , args.lr ,args.beta1, args.beta2 , args.M, args.slurmid)
# NN_object.df = pd.read_csv(args.filename)
# =============================================================================

#Intialize the neural network object's fields
NN_object = Neural_Net(None, 195, 55, 1, 0.00007, 0.93 , 0.97 , 0.96, 90001) 
NN_object.df = pd.read_csv('snap3_training_dataset_splits_15_additional_features_plus_snap_and_pmd.csv.gz')
NN_object.df.PMD = NN_object.df.PMD_detailed.map({'neutral': 0, 'mild': 0.3, 'moderate': 0.6, 'severe': 1})
sample_ids = pd.Series(range(1,len(NN_object.df) + 1))
sample_ids = sample_ids.to_frame() 
sample_ids.columns = ["sample_id"] 
NN_object.df = NN_object.df.merge(sample_ids, left_index = True , right_index = True)
print(NN_object.df)



""" Method which trains on the data for 200 rounds. Finds the best accuracy epoch and trains
for that much more divided by 2 epochs. Keeps training until reaches maximal accuracy. """
def continuous_training():
    rmse_test_array = []
    prediction_values = []
    actual_values = []
    NN_object.df = NN_object.df.sample(frac = 1)
    print(NN_object.df)
 #   kfold = KFold(10, False)     
  #  for train, test in kfold.split(NN_object.df):
    for split_id in range(1,11):
        split = NN_object.df[NN_object.df.split == split_id]
        train = NN_object.df[NN_object.df.split != split_id]
        # Split the dataset into training and testing
        sample_IDS, x_test_tensor , y_test_tensor, training_set , validation_set = NN_object.split(split , train)
    
        # Balance the validation and training datasets
        training_rand, validation_rand = NN_object.balance(training_set , validation_set)
    
        # Create the model using the appropriate data dimensions
        model = NN_object.model(NN_object.D_in,NN_object.H,NN_object.D_out)
    
        round_ = 0 
        new_min_index = 0 
        min_index = 0
        min_rmse = 0
        epochs = 0
        continueTraining = True
        rmse_array = []
        new_rmse_array = []
        while continueTraining: 
            round_ += 1
            
            # when training epoch is less than 200. Train and find accuracy
            if round_<= 200: 
                rmse = NN_object.training(round_ , model, training_rand , validation_rand)  
                rmse_array.append(rmse)
                
            # At the 200th epoch determine check if split index is 0. If yes
            # stop, if no detemrine extra epoch number
            if round_ == 200 :
                min_rmse = min(rmse_array)
                min_index = np.argmin(rmse_array) 
                print("The current round is " + str(round_))
                print("The max_Index is " + str(min_index))
                print("The min_rmse is " + str(min_rmse))
                split_min_index = int(min_index / 2)
                epochs = 200 + split_min_index
                if (split_min_index == 0): 
                    epochs = 300
                
            # if round > 200 continue training by the amount determined
            # in the previous conditional statement
            if round_ > 200 and round_ <= epochs: 
                rmse = NN_object.training(round_ , model, training_rand , validation_rand)  
                new_rmse_array.append(rmse)
            # When rounds equals epochs, determine if more training is necessary and how much
            if round_ == epochs : 
                new_min_rmse = min(new_rmse_array)
                new_min_index = np.argmin(new_rmse_array)
                split_min_index = int(new_min_index / 2)
                new_rmse_array = []
                print("The current round is " + str(round_))
                print("new_min_rmse is " + str(new_min_rmse))
                print("min rmse is " + str(min_rmse))
                # If max accuracy is at the first or 2nd epoch
                # No more training is necessary
                if (split_min_index == 0) : 
                    break
                # If the new max accuracy <= the older max accuracy
                # exit the loop
                if new_min_rmse >= min_rmse:
                    continueTraining = False
                    print(continueTraining)
                # Else make the variable max_acc assigned new_max_acc
                else:
                    print(continueTraining)
                    min_rmse = new_min_rmse
                    epochs += split_min_index
        x_test = x_test_tensor    
        y_test = y_test_tensor
        
        # Find accuracy on test set for each fold of CV
        rmse_loss , pred, actual_value = NN_object.testing(split_id, model, x_test,y_test)
        prediction_values.extend(pred)
        actual_values.extend(actual_value)
        rmse_test_array.append(rmse_loss)
# =============================================================================
#         test , prob, true_p, false_p ,true_n , false_n, pos_recall , neg_recall , pos_pres , neg_pres , balan_acc , MCC , f1 ,ROC_AUC, pres_recall_AUC, accuracy = NN_object.testing(model, x_test, y_test)
#         # Calculate overall true positives,false positives, true negatives and false negatives
#         # Add each performance metric from a fold to an array of performance metrics.
#         tp += true_p
#         fp += false_p
#         tn += true_n
#         fn += false_n
#         true_labels.extend(test)
#         prob_scores.extend(prob)
#         testing_accuracy_array.append(accuracy)
#         pos_recall_array.append(pos_recall)
#         neg_recall_array.append(neg_recall)
#         pos_pres_array.append(pos_pres)
#         neg_pres_array.append(neg_pres)
#         balanc_acc_array.append(balan_acc)
#         MCC_array.append(MCC)
#         f1_array.append(f1)
#         ROC_AUC_array.append(ROC_AUC)
#         p_r_AUC_array.append(pres_recall_AUC)
# =============================================================================
        
    # Print out all the final performance metrics
    NN_object.Results(rmse_test_array, prediction_values , actual_values) 
    
if __name__ == "__main__":
    continuous_training()                 